var pc=$('#pc');
var block = '█';
var ctx;
var bbcode=$('#bbcode').click(function() {
	this.select();
});

function myalert(msg) {
	$('<div title="error">'+
		'<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+msg+'</p>'+
	'</div>').dialog({ modal: true, buttons: { 'OK': function() { $(this).dialog('close'); } } });
}

if ($.browser.msie) {
	myalert('You\'re using Internet Explorer. Please get the latest Version of <a href="http://www.mozilla.com/firefox/">Firefox</a>, <a href="http://www.google.com/chrome">Chrome</a> or <a href="http://www.opera.com/">Opera</a> to use this.');
} else if (typeof FileReader == 'undefined') {
	myalert('Your Browser doesn\'t support direct access to pictures. Please get the latest Version of <a href="http://www.mozilla.com/firefox/">Firefox</a>, <a href="http://www.google.com/chrome">Chrome</a> or <a href="http://www.opera.com/">Opera</a> to use this.');
}



function Color() {
	this.init.apply(this,arguments);
}
Color.prototype={
	init: function(r,g,b,a) {
		this.r=r;
		this.g=g;
		this.b=b;
		if (typeof a == 'undefined')
			this.a=255;
		else
			this.a=a;
		return this;
	},
	cmp: function(color) {
		if (color.a==this.a&&color.r==this.r&&color.g==this.g&&color.b==this.b)
			return true;
		return false;
	},
	hex: function() {	
		if (this.a == 0)
			return 'transparent';
		var r='';
		var rgb=[this.r,this.g,this.b];
		for(var i in rgb) {
			var t = rgb[i].toString(16);
			r += (t.length <= 1 ? '0' : '')+t;
		};
		return '#'+r;
	},
	fromData: function(x,y,data) {
		var i = 4 * (y * data.width + x);
		return new Color(data.data[i],data.data[i+1],data.data[i+2],data.data[i+3]);
	},
	fromHex: function(str) {
		var m=str.match(/^#([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i).slice(1);
		var rgb=[];
		for(var i in m) {
			rgb[i]=parseInt(m[i],16);
		}
		var c=new Color();
		c.init.apply(c,rgb);
		return c;
	}
};

function Output(elem) {
	this.elem=elem;
	this.lastColor=null;
	this.lastElement=null;
	this.bbcode='';
	this.transparentColor=Color.prototype.fromHex('#2E3B41');
	this.currentLine=[];
}
Output.prototype={
	push: function(color) {
		this.currentLine.push(color);
	},
	_push: function(color) {
		this.lastElement=this.appendDomElement(color);
		this.appendBBCodeElement(color);
		this.lastColor=color;
	},
	appendDomElement: function(color) {
		var r;
		if (this.lastColor && this.lastColor.cmp(color) && this.lastElement) {
			r = this.lastElement.append(document.createTextNode(block));
		} else {
			this.elem.append(r=$('<span>'+block+'</span>').css('color',color.hex()));
		}
		return r;
	},
	appendBBCodeElement: function(color) {
		this.bbcode+=this.openTag(color)+block;
	},
	openTag: function(color) {
		if (this.lastColor && this.lastColor.cmp(color))
			return '';
		return this.closeTag()+'[color='+color.hex()+']';
	},
	closeTag: function() {
		if (this.lastColor) {
			this.lastColor = null;
			return '[/color]';
		}
		return '';
	},
	newLine: function() {
		if (this.currentLine.length > 0) {
			if (this.currentLine.length % 2 == 0) {
				this.currentLine.pop();
			}
			var left=0;
			var right = this.currentLine.length-1;
			while (this.isTransparent(this.currentLine[left]) && left < right) left++;
			while (this.isTransparent(this.currentLine[right]) && right >= left) right--;
			var middle = Math.floor(this.currentLine.length/2);
			
			if (left != this.currentLine.length - 1) {
				if (middle - left > right - middle) {
					right = middle + (middle - left);
				} else {
					left = middle - (right - middle);
				}
				this.currentLine = this.currentLine.slice(left,right);
				
				for(var i in this.currentLine) {
					this._push(this.currentLine[i]);
				}
			}
			this.currentLine=[];
		}
		
		// for(var i in this.currentLine) {
			// this._push(this.currentLine[i]);
		// }
		// this.currentLine=[];
		
		this.bbcode+="\n";
		this.elem.append($('<br/>'));
		this.lastElement=null;
	},
	isTransparent: function(color) {
		return this.transparentColor.cmp(color) || color.a==0;
	},
	result: function() {
		this.bbcode = this.bbcode.replace(/\s+$/,'') + this.closeTag();
		this.lastColor=null;
		this.lastElement=null;
		return "[center]"+this.bbcode.replace(/\s*$/,'')+"\npixels from http://morgh.us/pixelate[/center]";
	}
};

String.prototype.bytes = function() {
	var len = this.length;
	var ret = 0;
	for(var i = 0; i < len; i++) {
		var c = this.charCodeAt(i);
		if (c < 128) {
			ret++;
		} else if((c > 191) && (c < 224)) {
			ret += 2;
		} else {
			ret += 3;
		}
	}
	return ret;
};

var cropvalue;

function doImage(img,c) {
	if (!c) {
		c = {};
		c.w = img.width;
		c.h = img.height;
		c.x = 0;
		c.y = 0;
	}
	var squishFactor = 0.6;
	var targetSize = [pc.width(),pc.height()];
	var aspect=c.w/c.h;
	var size=[];
	var resizeFactor;
	if (aspect > 1) {
		size[0]=targetSize[0];
		size[1]=targetSize[1]/aspect;
		resizeFactor=size[0]/c.w;
	} else {
		size[0]=targetSize[0]*aspect;
		size[1]=targetSize[1];
		resizeFactor=size[1]/c.h;
	}
	ctx.save();
	ctx.clearRect(0,0,targetSize[0],targetSize[1]);
	ctx.scale(resizeFactor,resizeFactor*squishFactor);
	ctx.drawImage(img,-c.x,-c.y);
	
	var o = new Output($('#output').html(''));

	var i;
	var id = ctx.getImageData(0,0,size[0],size[1]*squishFactor), idd=id.data;
	for (var y = 0; y < id.height; y++) {
		for (var x = 0; x < id.width; x++) {
			px=Color.prototype.fromData(x,y,id);
			o.push(px);
		}
		o.newLine();
	}
	var res=o.result();
	bbcode.val(res);
	var bytes = res.bytes();
	$('#fawarning').toggle(bytes > 49595);
	$('#charsleft').text(49595-bytes);
	$('#charsleftcont').toggle(bytes <= 49595);
	ctx.restore();
}

ctx=pc[0].getContext('2d');
ctx.mozImageSmoothingEnabled = true;

var slider=$("#resslider");
var sliderTimeout = null;
function setSlider() {
	var res=slider.slider('value');
	clearTimeout(sliderTimeout);
	sliderTimeout = setTimeout(function() {
		pc.attr('width',res);
		pc.attr('height',res);
		doImage(lastImg[0],cropvalue);
	},100);
}
slider.slider({
	min: 10,
	max: 100,
	change: setSlider,
	slide: setSlider,
	value: pc.width(),
});

var uploadform=$('#uploadform');
$(document.body).append($('<iframe width="0" height="0" name="uploadformtarget" style="display:none">'));
uploadform.attr('target','uploadformtarget');
uploadform.find('input[type=file]').change(function() {
	var file;
	if (typeof this.files != 'undefined' && this.files.length > 0) {
		file = this.files[0];
		var img = $('<img/>');
		lastImg = img;
		img.load(function() {
			cropvalue = null;
			
			doImage(this);
			
			var maxsize = 200;
			
			var that = this;	
			var clonedImage = $(this).clone();
			var imagesize=[this.width,this.height];
			if (this.width > this.height) {
				clonedImage.css('width',maxsize);
				var scalefactor = maxsize / this.width;
			} else {
				clonedImage.css('height',maxsize);
				var scalefactor = maxsize / this.height;
			}		
			

			var timeout;
			var changeFunc = function(c) {
				clearTimeout(timeout);
				timeout = setTimeout(function() {
					$.each(['x','y','x2','y2','w','h'],function() {
						c[this]/=scalefactor;
					});
					if (c.h == 0 && c.w == 0)
						c = null;
					cropvalue = c;
					doImage(that,c);
				},100);
			};
			
			$('#preview').html('').append(clonedImage);
			clonedImage.Jcrop({
				trackDocument: true,
				onSelect: changeFunc,
				onChange: changeFunc
			});
		});
		img.error(function() {
			myalert('could not display image. is it an image?');
		});
		var reader = new FileReader();
		reader.onload = function(evt) {
			img[0].src = evt.target.result;
		};
		reader.readAsDataURL(file);
				
	} else {
		myalert('browser cannot access the file. try upgrading?');
	}
});

$('#feedbackdialog').dialog({
	modal: true,
	autoOpen: false,
	buttons: {
		'send': function() {
			$(this).dialog('close');
			var progress = $('<div title"sending...">please wait</div>').dialog({ modal: true });
			$.ajax({
				url: 'feedback.php',
				type: 'POST',
				data: $(this).find('form').serialize(),
				success: function(response) {
					progress.dialog('close');
					$('<div title="thanks">message sent</div>').dialog({ modal: true, buttons: { 'OK': function() { $(this).dialog('close'); } } });
				}
			});
		},
		'cancel': function() {
			$(this).dialog('close');
		}
	}
});
$('#feedback').click(function() {
	$('#feedbackdialog').dialog('open');
});
$('button').button();