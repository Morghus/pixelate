<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>pixelate</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="jcrop/jquery.Jcrop.css" />
		<link rel="stylesheet" type="text/css" href="s.css?2" />
		<link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.7.0/themes/ui-darkness/jquery-ui.css" />
		<script type="text/javascript">

		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-4651410-1']);
		  _gaq.push(['_trackPageview']);

		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>
	</head>
	<body>
		<div id="wrapper">		
			<div id="sidebarbg"></div>
			<div id="sidebar">
				<div id="sidebarcont">
					<h1>Pixel Profile Generator</h1>
					<fieldset style="position: relative">
						<legend>image</legend>
						<div id="previewcont">
							<span id="preview">
							</span>
						</div>
						<form id="uploadform" action="upload.php" method="post" enctype="multipart/form-data">
							<span class="upload">
								<button>choose file</button>
								<input type="file" name="image" />
							</span>
						</form>
						<ul>
							<li>resize your picture before choosing it here, it'll yield better results (optimal size: 60x60 pixel)</li>
							<li>GIF and PNG transparency supported</li>
						</ul>
					</fieldset>
					<fieldset>
						<legend>resolution</legend>
						<div style="margin: 1em">
							<div id="resslider"></div>
						</div>
					</fieldset>
					<p>
						<table class="creds">
							<tr>
								<th>coded by</th>
								<td><a href="http://www.furaffinity.net/user/morghus"><!--img border="0" width="50" height="50" src="av.php?morghus.gif" align="middle" /--> morghus</a></td>
							</tr>
							<tr>
								<th>pixel profile idea by</th>
								<td><a href="http://www.furaffinity.net/user/ryoken"><!--img border="0" width="50" height="50" src="av.php?ryoken.gif" align="middle" /--> ryoken</a></td>
							</tr>
						</table>
						<hr/>
						Copyright &copy; 2011 &nbsp;&bull;&nbsp; <a href="javascript:;" id="feedback">give feedback</a>
					</p>
				</div>
			</div>
			<div id="page">
				<canvas id="pc" width="63" height="63" style="display:none">
					Your Browser Doesn't Support The &lt;canvas&gt; Element.<br/>
					Get <a href="http://www.google.com/chrome">Chrome</a> or <a href="http://www.mozilla.com/firefox">Firefox</a>.
				</canvas>
				<div id="output"></div>
				<fieldset>
					<legend>cut and paste to your profile</legend>
					<p id="fawarning" class="ui-widget ui-corner-all ui-state-error" style="display:none">Warning, code is longer than FAs maximum profile size.</p>
					<p id="charsleftcont" style="display:none">Characters left for other stuff: <span id="charsleft"></span></p>
					<textarea readonly="readonly" id="bbcode" cols="100" rows="6" class="text ui-widget-content ui-corner-all"></textarea>
					<p>you are free to remove the link back to this site from the last line, however I would very much appreciate it if you leave it there.</p>
				</fieldset>
			</div>
			<div style="display: none" id="feedbackdialog" title="feedback">
				<p>got a problem? like it? hate it? leave me a message here</p>
				<form>
					<label for="email">e-mail (optional)</label>
					<input type="text" id="email" name="email" class="text ui-widget-content ui-corner-all"/><br/>
					<label for="comment">message</label>
					<textarea id="comment" name="text" cols="30" rows="4" onfocus="if(this.value=='message')this.value=''" class="text ui-widget-content ui-corner-all"></textarea><br/>
				</form>
			</div>
		</div>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/jquery-ui.min.js"></script>
		<script type="text/javascript" src="jcrop/jquery.color.js"></script>
		<script type="text/javascript" src="jcrop/jquery.Jcrop.min.js"></script>
		<script src="pixelate.js?5" type="text/javascript"></script>
	</body>
</html>
