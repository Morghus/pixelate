function HSVobject (hue, saturation, value) {
	// Object definition.
	this.h = hue; this.s = saturation; this.v = value;
	this.validate = function () {
		if (this.h < 0) {this.h += 360;}
		if (this.s < 0) {this.s = 0;}
		if (this.v < 0) {this.v = 0;}
		if (this.h > 360) {this.h = this.h % 360;}
		if (this.s > 100) {this.s = 100;}
		if (this.v > 100) {this.v = 100;}
	};
}

function RGBobject (red, green, blue) {
	// Object definition.
	this.r = red; this.g = green; this.b = blue;
	this.validate = function () {
		if (this.r <= 0) {this.r = 0;}
		if (this.g <= 0) {this.g = 0;}
		if (this.b <= 0) {this.b = 0;}
		if (this.r > 255) {this.r = 255;}
		if (this.g > 255) {this.g = 255;}
		if (this.b > 255) {this.b = 255;}
	};
}


function RGB2HSV (RGB, HSV) {
	r = RGB.r / 255; g = RGB.g / 255; b = RGB.b / 255; // Scale to unity.

	var minVal = Math.min(r, g, b);
	var maxVal = Math.max(r, g, b);
	var delta = maxVal - minVal;

	HSV.v = maxVal;

	if (delta == 0) {
		HSV.h = 0;
		HSV.s = 0;
	} else {
		HSV.s = delta / maxVal;
		var del_R = (((maxVal - r) / 6) + (delta / 2)) / delta;
		var del_G = (((maxVal - g) / 6) + (delta / 2)) / delta;
		var del_B = (((maxVal - b) / 6) + (delta / 2)) / delta;

		if (r == maxVal) {HSV.h = del_B - del_G;}
		else if (g == maxVal) {HSV.h = (1 / 3) + del_R - del_B;}
		else if (b == maxVal) {HSV.h = (2 / 3) + del_G - del_R;}
		
		if (HSV.h < 0) {HSV.h += 1;}
		if (HSV.h > 1) {HSV.h -= 1;}
	}
	HSV.h *= 360;
	HSV.s *= 100;
	HSV.v *= 100;
}

function HSV2RGB (HSV, RGB) {
	var h = HSV.h / 360; var s = HSV.s / 100; var v = HSV.v / 100;
	if (s == 0) {
		RGB.r = v * 255;
		RGB.g = v * 255;
		RGB.v = v * 255;
	} else {
		var_h = h * 6;
		var_i = Math.floor(var_h);
		var_1 = v * (1 - s);
		var_2 = v * (1 - s * (var_h - var_i));
		var_3 = v * (1 - s * (1 - (var_h - var_i)));
		
		if (var_i == 0) {var_r = v; var_g = var_3; var_b = var_1}
		else if (var_i == 1) {var_r = var_2; var_g = v; var_b = var_1}
		else if (var_i == 2) {var_r = var_1; var_g = v; var_b = var_3}
		else if (var_i == 3) {var_r = var_1; var_g = var_2; var_b = v}
		else if (var_i == 4) {var_r = var_3; var_g = var_1; var_b = v}
		else {var_r = v; var_g = var_1; var_b = var_2};
		
		RGB.r = var_r * 255;
		RGB.g = var_g * 255;
		RGB.b = var_b * 255;
	}
}




var canvas = $('test');

var c2 = new Element('canvas', { width: 400, height: 558 })
var ctx2 = c2.getContext('2d');

var ctx = canvas.getContext('2d');

var img = new Element('img').addEvent('load', function(event) {
	ctx2.drawImage(img , 0, 0);
	redrawImage();
}).set('src', 'waterdrake.png');



function redrawImage() {

	var imageData = ctx2.getImageData(0, 0, c2.width, c2.height);

	var index;
	var r,g,b,a;
	var y;
	var rgb;
	var hsv;
	
	for (var x = 0; x < imageData.width; x++) {
		for (y = 0; y < imageData.height; y++) {
			index = 4 * (y * imageData.width + x);
			r = imageData.data[index];
			g = imageData.data[index + 1];
			b = imageData.data[index + 2];
			a = imageData.data[index + 3];
			
			if (a == 0)
				continue;
			
			rgb = new RGBobject(r,g,b);
			hsv = new HSVobject(0,0,0);
			RGB2HSV(rgb,hsv);
			hsv.h += hue_adjust;
			hsv.s += saturation_adjust;
			hsv.v += brightness_adjust;
			hsv.validate();
			HSV2RGB(hsv,rgb);
			
			imageData.data[index] = rgb.r;
			imageData.data[index + 1] = rgb.g;
			imageData.data[index + 2] = rgb.b;

		}
	}

	ctx.putImageData(imageData, 0, 0);
}


var CanvasImage = new Class({
	Implements: [Options],
	options: {
		src: undefined,
		width: 0,
		height: 0
	},
	width: 0,
	height: 0,
	initialize: function(options) {
		this.setOptions(options);
	}
});

/* create control */
var hue_adjust = 0;
var saturation_adjust = 0;
var brightness_adjust = 0;

var timer;

new Slider($('hue'), $('hue').getElement('.knob'), {
	range: [0,360],	// Minimum value is 8
	onChange: function(value){
		hue_adjust=value;
		$clear(timer);
		timer = redrawImage.delay(0);
	}
});
new Slider($('saturation'), $('saturation').getElement('.knob'), {
	range: [-100,100],	// Minimum value is 8
}).set(0).addEvent('change', function(value){
	saturation_adjust=value;
	$clear(timer);
	timer = redrawImage.delay(0);
});
new Slider($('brightness'), $('brightness').getElement('.knob'), {
	range: [-100,100],	// Minimum value is 8
}).set(0).addEvent('change', function(value){
	brightness_adjust=value;
	$clear(timer);
	timer = redrawImage.delay(0);
});